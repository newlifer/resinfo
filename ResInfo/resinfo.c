
#include "resinfo.h"

#ifdef __linux__

#include <stdio.h>
#include <unistd.h>
#include <sys/sysinfo.h>
#include <sys/resource.h>
#include <sched.h>
#include <string.h>

//NOTE: linux version relies on external utilities such as grep, ps and vmstat. We are working on dependencies' decrease

#elif defined _WIN32

#ifdef WINVER
  #undef WINVER
#endif
#define WINVER 0x0500

#include "windows.h"
#include "psapi.h"

#elif defined TARGET_OS_MAC
    #error Cant be compiled on Mac yet
#endif

unsigned long 
#ifdef __cplusplus
resinfo::
#endif
totalVirtualMemory()
{
#ifdef _WIN32
    MEMORYSTATUSEX memInfo;
    memInfo.dwLength = sizeof(MEMORYSTATUSEX);
    GlobalMemoryStatusEx(&memInfo);
    return (unsigned long)(memInfo.ullTotalPageFile);
#elif defined __linux__
    FILE* p = popen("vmstat -s | grep 'total memory'", "r");
    if (!p)
        return 0;
    char buf[128];
    fgets(buf, sizeof(buf), p);
    pclose(p);
    unsigned long memSize = 0;
    sscanf(buf, "%lu", &memSize);
    return memSize * 1024u;
#endif
}

unsigned long
#ifdef __cplusplus
resinfo::
#endif
currentVirMemoryUsed()
{
#ifdef _WIN32
    MEMORYSTATUSEX memInfo;
    memInfo.dwLength = sizeof(MEMORYSTATUSEX);
    GlobalMemoryStatusEx(&memInfo);
    return (unsigned long)(memInfo.ullTotalPageFile - memInfo.ullAvailPageFile);
#elif defined __linux__
    FILE* p = popen("vmstat -s | grep 'used memory'", "r");
    if (!p)
        return 0;
    char buf[128];
    fgets(buf, sizeof(buf), p);
    pclose(p);
    unsigned long memSize = 0;
    sscanf(buf, "%lu", &memSize);
    return memSize * 1024u;
#endif
}

unsigned long
#ifdef __cplusplus
resinfo::
#endif
currentVirtMemoryByProcess()
{
#ifdef _WIN32
    PROCESS_MEMORY_COUNTERS_EX pmc;
    pmc.cb = sizeof(pmc);
    //GetProcessMemoryInfo(GetCurrentProcess(), (PROCESS_MEMORY_COUNTERS*)&pmc, pmc.cb);
    return (unsigned long)(pmc.PrivateUsage);
#elif defined __linux__
    pid_t mypid = getpid();
    char str[64] = "cat /proc/";
    sprintf(str+strlen(str), "%d", mypid);
    strcat(str, "/status | grep VmSize:");
    FILE* p = popen(str, "r");
    if (!p)
        return 0;
    char buf[256];
    fgets(buf, sizeof(buf), p);
    pclose(p);
    unsigned long vsize;
    sscanf(buf, "VmSize: %lu", &vsize);
    return vsize * 1024u;
#endif
}

unsigned long
#ifdef __cplusplus
resinfo::
#endif
totalPhysicalMemory()
{
#ifdef _WIN32
    MEMORYSTATUSEX memInfo;
    memInfo.dwLength = sizeof(MEMORYSTATUSEX);
    GlobalMemoryStatusEx(&memInfo);
    return (unsigned long)(memInfo.ullTotalPhys);
#elif defined __linux__
    struct sysinfo info;
    sysinfo(&info);
    return info.totalram * info.mem_unit;
#endif
}



unsigned long
#ifdef __cplusplus
resinfo::
#endif
currentPhysMemoryUsed()
{
#ifdef _WIN32
    MEMORYSTATUSEX memInfo;
    memInfo.dwLength = sizeof(MEMORYSTATUSEX);
    GlobalMemoryStatusEx(&memInfo);
    return (unsigned long)(memInfo.ullTotalPhys - memInfo.ullAvailPhys);
#elif defined __linux__
    struct sysinfo info;
    sysinfo(&info);
    return (info.totalram - info.freeram) * info.mem_unit;
#endif
}

unsigned long
#ifdef __cplusplus
resinfo::
#endif
currentPhysMemoryByProcess()
{
#ifdef _WIN32
    PROCESS_MEMORY_COUNTERS_EX pmc;
    pmc.cb = sizeof(pmc);
    //GetProcessMemoryInfo(GetCurrentProcess(), (PROCESS_MEMORY_COUNTERS*)&pmc, pmc.cb);
    return (unsigned long)(pmc.WorkingSetSize);
#elif defined __linux__
    unsigned long rss = 0;
	FILE* f = fopen( "/proc/self/statm", "r" );
	if (!f)
		return 0;
	if (fscanf( f, "%*s%ld", &rss ) != 1)
	{
		fclose(f);
		return 0;
	}
	fclose(f);
	return rss * sysconf(_SC_PAGESIZE);
#endif
}

float
#ifdef __cplusplus
resinfo::
#endif
cpuCurrentUsed()
{
#ifdef __linux__
    FILE* p = popen("cat /proc/loadavg", "r");
    if (!p)
        return 0;
    char buf[64];
    fgets(buf, sizeof(buf), p);
    pclose(p);
    float cpu = 0.0f;
    sscanf(buf, "%f", &cpu);
    fflush(stdout);

    cpu_set_t cs;
    CPU_ZERO(&cs);
    sched_getaffinity(0, sizeof(cs), &cs);

    int count = 0;
    for (int i = 0; i < 8; i++)
    {
        if (CPU_ISSET(i, &cs))
            count++;
    }

    return cpu/count*100;
#elif defined _WIN32
    return 0;
#endif
}

float 
#ifdef __cplusplus
resinfo::
#endif
cpuCurrentByProcess()
{
#ifdef __linux__
    pid_t mypid = getpid();
    char str[64] = "ps -p ";
    sprintf(str+strlen(str), "%d", mypid);
    strcat(str, " -o %cpu");

    FILE* p = popen(str, "r");
    if (!p)
        return 0;
    char buf[8];
    fgets(buf, sizeof(buf), p);
    fgets(buf, sizeof(buf), p);
    float cpu;
    sscanf(buf, "%f", &cpu);
    pclose(p);
    return cpu;
#elif defined _WIN32
    return 0;
#endif
}






