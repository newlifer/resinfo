#! /bin/bash

g++ -D_GNU_SOURCE -c -Wall -o resinfo.o resinfo.c
ar rcs libresinfocpp.a resinfo.o
g++ main.c -Wall -L. -lresinfocpp -o cppmain

rm resinfo.o
