#include "resinfo.h"
#include <stdio.h>

#ifdef __cplusplus
using namespace resinfo;
#endif

int main()
{
    printf("%lu %lu %lu\n", totalPhysicalMemory(), currentPhysMemoryUsed(), currentPhysMemoryByProcess());
    printf("%lu %lu %lu\n", totalVirtualMemory(), currentVirMemoryUsed(), currentVirtMemoryByProcess());
    printf("%f %f\n", cpuCurrentUsed(), cpuCurrentByProcess());
    return 0;
}