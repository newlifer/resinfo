#! /bin/bash

gcc -std=gnu99 -D_GNU_SOURCE -c -Wall -o resinfo.o resinfo.c
ar rcs libresinfo.a resinfo.o
gcc main.c -Wall -L. -lresinfo -o cmain

rm resinfo.o
