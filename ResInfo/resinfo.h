#ifndef RESINFO_H
#define RESINFO_H

#ifdef __cplusplus
namespace resinfo {
#endif

/**********************************************************************************************************
 *
 *  The following functions are for:
 *    — Total virtual memory available
 *    — Virtual memory currently used
 *    — Virtual memory currently used by my process
 *
 *    — Total RAM available
 *    — RAM currently used
 *    — RAM currently used by my process
 *
 *    — CPU currently used
 *    — CPU currently used by my process
 *
 * ************************************************************************************************************/

/**
 * @brief totalVirtualMemory
 * @return
 */
extern unsigned long totalVirtualMemory();

/**
 * @brief currentVirMemoryUsed
 * @return
 */
extern unsigned long currentVirMemoryUsed();

/**
 * @brief currentVirtMemoryByProcess
 * @return
 */
extern unsigned long currentVirtMemoryByProcess();

/**
 * @brief totalPhisicalMemory
 * @return
 */
extern unsigned long totalPhysicalMemory();

/**
 * @brief currentPhisMemoryUsed
 * @return
 */
extern unsigned long currentPhysMemoryUsed();

/**
 * @brief currentPhisMemoryByProcess
 * @return
 */
extern unsigned long currentPhysMemoryByProcess();

/**
 * @brief cpuCurrentUsed
 * @return
 */
extern float cpuCurrentUsed();

/**
 * @brief cpuCurrentByProcess
 * @return
 */
extern float cpuCurrentByProcess();

#ifdef __cplusplus
}
#endif

#endif // RESINFO_H




























